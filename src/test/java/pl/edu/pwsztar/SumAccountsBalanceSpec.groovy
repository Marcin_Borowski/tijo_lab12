package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountsBalanceSpec extends Specification {
    @Unroll
    def "should show sum of all accounts balances "() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1, 140)
            bank.deposit(2, 200)
            bank.deposit(3, 250)
        when: "Total balance of accounts is shown"
            def totalbalance = bank.sumAccountsBalance()
        then: "Should return total account balance"
            totalbalance == 590
    }

    def "Should return 0 if no accounts "() {
        given: "initial data"
            def bank = new Bank()
        when: "Total balance of accounts is shown"
            def totalbalance = bank.sumAccountsBalance()
        then: "Should return total account balance"
            totalbalance == 0
    }
}
