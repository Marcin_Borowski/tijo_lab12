package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification {

    @Unroll
    def "should deposit on account number #accNum "() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
        when: "Account of #accNum got deposit"
            def deposit = bank.deposit(accNum,cashdeposit)
            bank.deposit(accNum,cashdeposit)
            bank.deposit(accNum,cashdeposit)
        then: "Should return account balance"
            deposit == success
        where:
            accNum   |cashdeposit   || success
            1        |100           || true
            2        |200           || true
            3        |300           || true
            5        |500           || false
    }
}
