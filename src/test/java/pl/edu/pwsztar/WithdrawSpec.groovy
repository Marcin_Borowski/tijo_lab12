package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification {

    @Unroll
    def "should withdraw on account number #accNum "() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1,140)
            bank.deposit(2,200)
            bank.deposit(3,250)
        when: "Account of #accNum got withdrawal"
        def withdraw = bank.withdraw(accNum,withdrawcash)
            bank.withdraw(accNum,withdrawcash)
            bank.withdraw(accNum,withdrawcash)
            bank.withdraw(accNum,withdrawcash)
        then: "Should return if operation is succesfull "
            withdraw == success
        where:
            accNum   |withdrawcash   || success
            1        |100           || true
            2        |200           || true
            3        |300           || false
            5        |500           || false
    }
}
