package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification{

    @Unroll
    def "should show balance of account number #accNum "() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1,140)
            bank.deposit(2,200)
            bank.deposit(3,250)
        when: "Balance of #accNum is shown"
            def showbalance = bank.accountBalance(accNum)
            bank.accountBalance(accNum)
            bank.accountBalance(accNum)
            bank.accountBalance(accNum)
        then: "Should return account balance"
            showbalance == balance
        where:
            accNum   || balance
            1        || 140
            2        || 200
            3        || 250
            5        || -1
    }
}

