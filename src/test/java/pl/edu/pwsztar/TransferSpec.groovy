package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification {

    @Unroll
    def "should transfer between #accNum and #accNum2"() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
            bank.deposit(1,140)
            bank.deposit(2,200)
            bank.deposit(3,250)
        when: "Accounts of #accNum and #accNum2 got transfer"
            def transfered = bank.transfer(accNum,accNum2,transfer)
            bank.transfer(accNum,accNum2,transfer)
            bank.transfer(accNum,accNum2,transfer)
        then: "Should return if operation is succesfull "
            transfered == success
        where:
            accNum   |accNum2     |transfer      || success
            1        |2           |100           || true
            2        |1           |200           || true
            3        |1           |300           || false
            5        |3           |500           || false
    }
}
