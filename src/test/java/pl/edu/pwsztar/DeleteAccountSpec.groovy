package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "should delete account number #accNum "() {
        given: "initial data"
            def bank = new Bank()
            bank.createAccount()
            bank.createAccount()
            bank.createAccount()
        when: "Account of #accNum is deleted"
            def deletedbalance = bank.deleteAccount(accNum)
        then: "Should return account balance"
            deletedbalance == balance
        where:
            accNum   || balance
            1        || 0
            2        || 0
            3        || 0
            5        || -1
    }
}
