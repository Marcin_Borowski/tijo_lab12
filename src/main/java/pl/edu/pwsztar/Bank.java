package pl.edu.pwsztar;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
import java.util.ArrayList;
import java.util.List;

class Bank implements BankOperation {

    private int accountNumber = 0;
    private List<Account> createdaccounts = new ArrayList<>();

    public int createAccount() {
        accountNumber++;
        createdaccounts.add(new Account(accountNumber,0));
        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int accnull = AccountExists(accountNumber);
        if(accnull == -2){
            return ACCOUNT_NOT_EXISTS;
        }
        else {
            int balance = createdaccounts.get(accnull).getAccountBalance();
            createdaccounts.remove(accnull);
            return balance;
        }
    }

    public boolean deposit(int accountNumber, int amount) {
        int accnull = AccountExists(accountNumber);
        if(accnull == -2){
            return false;
        }
        else {
            createdaccounts.get(accnull).setAccountBalance(createdaccounts.get(accnull).getAccountBalance()+amount);
            return true;
        }
    }

    public boolean withdraw(int accountNumber, int amount) {
        int accnull = AccountExists(accountNumber);
        if(accnull == -2){
            return false;
        }
        else if(amount>createdaccounts.get(accnull).getAccountBalance()){
            return false;
        }
        createdaccounts.get(accnull).setAccountBalance(createdaccounts.get(accnull).getAccountBalance()-amount);
        return true;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        int accnull = AccountExists(fromAccount);
        int accnull2 = AccountExists(toAccount);
        if(accnull == -2 || accnull2== -2){
            return false;
        }
        else if(amount>createdaccounts.get(accnull).getAccountBalance()){
            return false;
        }
        createdaccounts.get(accnull).setAccountBalance(createdaccounts.get(accnull).getAccountBalance()-amount);
        createdaccounts.get(accnull2).setAccountBalance(createdaccounts.get(accnull2).getAccountBalance()+amount);
        return true;
    }

    public int accountBalance(int accountNumber) {
        int accnull = AccountExists(accountNumber);
        if(accnull == -2){
            return ACCOUNT_NOT_EXISTS;
        }
        else {
            return createdaccounts.get(accnull).getAccountBalance();
        }
    }

    public int sumAccountsBalance() {
        int sum=0;
        for(Account accounts : createdaccounts)
        {sum += accounts.getAccountBalance();}
        return sum;
    }

    private int AccountExists(int accountNumber) {
        int tempaccnum = 0;
        int accnull = -2;
        for (Account accounts : createdaccounts) {
            if (accounts.getAccountNumber() == accountNumber) {
                accnull = tempaccnum;
            }
            tempaccnum++;
        }
        return accnull;
    }
}
