package pl.edu.pwsztar;

public class Account {
    private int accountNumber;
    private int accountBalance;

    public Account(int accountNumber, int accountBalace) {
        this.accountNumber=accountNumber;
        this.accountBalance=accountBalace;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }
}
